<?php

/**
 * @file
 * Main module file for user_search module.
 */

/**
 * Implements hook_menu_alter().
 */
function user_search_menu_alter(&$items) {
  // User listing pages.
  // Use our own callback which in turns replaces the filter and admin forms.
  $items['admin/people']['page callback'] = 'user_search_admin';
}

/**
 * Replaces core user_admin().
 *
 * Almost identical to user_admin() but calls user_search_filter_form()
 * and user_search_admin_account() in place of the standard forms.
 */
function user_search_admin($callback_arg = '') {
  $op = isset($_POST['op']) ? $_POST['op'] : $callback_arg;

  switch ($op) {
    case t('Create new account'):
    case 'create':
      $build['user_register'] = drupal_get_form('user_register_form');
      break;

    default:
      if (!empty($_POST['accounts']) && isset($_POST['operation']) && ($_POST['operation'] == 'cancel')) {
        $build['user_multiple_cancel_confirm'] = drupal_get_form('user_multiple_cancel_confirm');
      }
      else {
        $build['user_filter_form'] = drupal_get_form('user_search_filter_form');
        $build['user_admin_account'] = drupal_get_form('user_search_admin_account');
      }
  }
  return $build;
}

/**
 * Replaces core user_admin_account().
 *
 * Form builder; User administration page.
 *
 * @ingroup forms
 * @see user_admin_account_validate()
 * @see user_admin_account_submit()
 */
function user_search_admin_account() {

  // Remove name_or_email filter from the sessions while calling core functions,
  // then put it back in.. to avoid undefined indices and broken SQL.
  if (isset($_SESSION['user_overview_filter'])) {

    foreach ($_SESSION['user_overview_filter'] as $index => $filter) {
      if ($filter[0] == 'name_or_email') {
        $stored_filter = $filter;
        $stored_filter_index = $index;
        unset($_SESSION['user_overview_filter'][$index]);
      }
    }

  }

  // Now generate the core form.
  $form = user_admin_account();

  // Put the filter back in the session, and now rebuild
  // relevant parts of the form.
  if (isset($stored_filter)) {
    $_SESSION['user_overview_filter'][$stored_filter_index] = $stored_filter;
  }

  // Now rerun the user query with name_or_email filter applied
  // and recreate relevant parts of the form.
  $destination = drupal_get_destination();

  $query = db_select('users', 'u');
  $query->condition('u.uid', 0, '<>');
  user_search_user_build_filter_query($query);

  $count_query = clone $query;
  $count_query->addExpression('COUNT(u.uid)');

  $query = $query->extend('PagerDefault')->extend('TableSort');

  $query
    ->fields('u', array('uid', 'name', 'status', 'created', 'access'))
    ->limit(50)
    // Get the header out of the existing form, so we don't have to rebuild it.
    ->orderByHeader($form['accounts']['#header'])
    ->setCountQuery($count_query);
  $result = $query->execute();

  // Begin directly copied from core code.
  $status = array(t('blocked'), t('active'));
  $roles = array_map('check_plain', user_roles(TRUE));
  $accounts = array();
  $options = array();
  foreach ($result as $account) {
    $users_roles = array();
    $roles_result = db_query('SELECT rid FROM {users_roles} WHERE uid = :uid', array(':uid' => $account->uid));
    foreach ($roles_result as $user_role) {
      $users_roles[] = $roles[$user_role->rid];
    }
    asort($users_roles);

    $options[$account->uid] = array(
      'username' => theme('username', array('account' => $account)),
      'status' =>  $status[$account->status],
      'roles' => theme('item_list', array('items' => $users_roles)),
      'member_for' => format_interval(REQUEST_TIME - $account->created),
      'access' =>  $account->access ? t('@time ago', array('@time' => format_interval(REQUEST_TIME - $account->access))) : t('never'),
      'operations' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('edit'),
          '#href' => "user/$account->uid/edit",
          '#options' => array('query' => $destination),
        ),
      ),
    );
  }
  // End directly copied from core code.
  // Replace the accounts in the form with our new set.
  $form['accounts']['#options'] = $options;

  // Rebuild the pager.
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;

}


/**
 * Replaces core user_filter_form().
 *
 * Form builder; Return form for user administration filters.
 *
 * @ingroup forms
 * @see user_search_filter_form_submit()
 */
function user_search_filter_form() {

  // Remove name_or_email filter from the sessions while calling core functions,
  // then put it back in.. to avoid undefindex indices and broken SQL.
  if (isset($_SESSION['user_overview_filter'])) {

    foreach ($_SESSION['user_overview_filter'] as $index => $filter) {
      if ($filter[0] == 'name_or_email') {
        $stored_filter = $filter;
        $stored_filter_index = $index;
        unset($_SESSION['user_overview_filter'][$index]);
      }
    }

  }

  // Now generate the core form.
  $form = user_filter_form();

  // Put the filter back in the session, and now rebuild
  // relevant parts of the form.
  if (isset($stored_filter)) {
    $_SESSION['user_overview_filter'][$stored_filter_index] = $stored_filter;
  }

  $session = isset($_SESSION['user_overview_filter']) ? $_SESSION['user_overview_filter'] : array();
  $filters = user_search_filters();
  foreach ($session as $filter) {
    list($type, $value) = $filter;

    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($type == 'name_or_email' && !empty($value)) {
      if (!empty($form['filters']['current'])) {
        $prepand = 'and ';
      }
      else {
        $prepand = '';
      }
      $form['filters']['current'][] = array('#markup' => t("$prepand%property contains %value", $t_args));
      unset($prepand);
    }
  }

  foreach ($filters as $key => $filter) {
    if ($key == 'name_or_email') {

      // Get the value.
      $value = '';
      foreach ($session as $session_filter) {
        if ($session_filter[0] == 'name_or_email') {
          $value = $session_filter[1];
        }
      }

      $form['filters']['status']['filters'][$key] = array(
        '#type' => 'textfield',
        '#title' => $filter['title'],
        '#size' => 40,
        '#default_value' => $value,
      );

    }
  }

  // As the core functions quite possible have detect no filters
  // because our filters are not expected or pass to the core functions
  // Re-add the relevant filter buttons to the form.
  $form['filters']['status']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['status']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => (count($session) ? t('Refine') : t('Filter')),
  );
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array(
      '#type' => 'submit',
      '#value' => t('Undo'),
    );
    $form['filters']['status']['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }

  return $form;
}

/**
 * Replaces core user_filters().
 *
 * Get the core user filters and add one extra.
 */
function user_search_filters() {
  // Regular filters.
  $filters = user_filters();

  // This is a non-standard filter.
  // It isn't displayed as a select so options is an empty array.
  // Also it applies to the name field and the email field, so we
  // specify 2 fields.
  $filters['name_or_email'] = array(
    'title' => t('name or email'),
    'field' => 'u.name',
    'field2' => 'u.mail',
    'options' => array(),
  );

  return $filters;
}


/**
 * Replaces core user_build_filter_query().
 *
 * Extends a query object for user administration filters based on session.
 *
 * @param SelectQuery $query
 *   Query object that should be filtered.
 */
function user_search_user_build_filter_query(SelectQuery $query) {
  $filters = user_search_filters();
  // Extend Query with filter conditions.
  foreach (isset($_SESSION['user_overview_filter']) ? $_SESSION['user_overview_filter'] : array() as $filter) {
    list($key, $value) = $filter;

    // This checks to see if this permission filter is an enabled permission for
    // the authenticated role. If so, then all users would be listed, and we can
    // skip adding it to the filter query.
    if ($key == 'permission') {
      $account = new stdClass();
      $account->uid = 'user_filter';
      $account->roles = array(DRUPAL_AUTHENTICATED_RID => 1);
      if (user_access($value, $account)) {
        continue;
      }
      $users_roles_alias = $query->join('users_roles', 'ur', '%alias.uid = u.uid');
      $permission_alias = $query->join('role_permission', 'p', $users_roles_alias . '.rid = %alias.rid');
      $query->condition($permission_alias . '.permission', $value);
    }
    elseif ($key == 'role') {
      $users_roles_alias = $query->join('users_roles', 'ur', '%alias.uid = u.uid');
      $query->condition($users_roles_alias . '.rid', $value);
    }
    elseif ($key == 'name_or_email') {
      $query->condition(db_or()->condition($filters[$key]['field'], '%' . $value . '%', 'LIKE')->condition($filters[$key]['field2'], '%' . $value . '%', 'LIKE'));
    }
    else {
      $query->condition($filters[$key]['field'], $value);
    }
  }
}


/**
 * Replaces core user_filter_form_submit().
 *
 * Process result from user administration filter form.
 */
function user_search_filter_form_submit($form, &$form_state) {

  user_filter_form_submit($form, $form_state);
  $op = $form_state['values']['op'];
  $filters = user_search_filters();
  switch ($op) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if ($filter == 'name_or_email' && isset($form_state['values'][$filter])) {
          $_SESSION['user_overview_filter'][] = array($filter, $form_state['values'][$filter]);
        }
      }
      break;
  }

  return;
}
